const IndexedSteps = new Proxy(Array, {
    construct(target, [args]) {
        idx = 0;
        args.forEach((item, idx) => (
            item.node = '',
            item.id = ++idx
        ))

        return new Proxy(target(...args),{
            get(arr, prop) {
                switch(prop) {
                    case 'first':
                        return arr.length > 0 ? arr[0] : void 0;
                    default:
                        return arr[prop];
                }
            }
        })
    }
});

const steps = new IndexedSteps([
    {title: 'Основы', text: 'В блоке вы познакомитесь со всеми основами Vue.js на практике. На протяжении блока мы напишем реактивное приложение, в процессе разработки которого разберем вся базу фреймворка.'},
    {title: 'Компоненты', text: 'Один из самых важных блоков в курсе, где вы узнаете все о компонентах. В блоке мы напишем 2 разных приложения и создадим более 5 различных UI компонентов как в реальной разработке. Блок расскажет про абсолютно все составляющие, которые есть в компонентах: взаимодействие, slots, асинхронные и динамические компоненты и тонна примеров.'},
    {title: 'Роутер', text: 'В данном блоке вы узнаете все о том, как работает мультиязычность во Vue. Мы создадим миниклон Gmail в данном блоке, где вы на практике увидите как работать с динамическим роутером.'},
    {title: 'Vuex', text: 'В блоке вы узнаете абсолютно все про Vuex. Вы узнаете как работать с данными, какие есть лучшие практики по их программированию и структурированию. Все на практике.'},
    {title: 'Composition', text: 'Одним из наиболее важных обновлений в Vue 3 является появление альтернативного синтаксиса Composition API. В этом блоке вы узнаете все, чтобы полностью пользоваться данными синтаксисом на практических примерах. Помимо этого вы узнаете как работать совместно с Vue Router и Vuex.'},
]);

const App = {
    data() {
        return {
            steps: steps,
            activeIndex: steps.first.id, // то, что позволяет определить текущий активный шаг
            activeSteps: [this.activeIndex],
            title: steps.first.text,
            backDisabled: 'disabled',
            endButtonShow: false,
            isDisabled: true,
        }
    },
    methods: {
        prev(idx) {
            // когда нажимаем кнопку назад
            idx--;
            if(idx >= 0) {
                this.activeIndex = idx
            } else {
                this.activeIndex = 1;
            }
            this.setActive(idx, true);
        },
        reset() {
            // начать заново
            this.setDefault();
        },
        nextOfFinish() {
            // кнопка вперед или закончить
            this.activeIndex++;
            this.showDesc(this.activeIndex);
            this.steps.forEach((item ) => (
                item.node = item.id < this.activeIndex
                    ? ' done'
                    : (item.id === this.activeIndex
                        ? ' active'
                        : '')
            ));
            this.isDisabled = this.backStatus();
            if(this.activeIndex === steps.length) {
                console.log('buttonChange');
                this.buttonChange();
            }
        },
        setActive(idx, back = false) {
            // когда нажимаем на определенный шаг
            this.showDesc(idx);
            this.activeIndex = idx;
            this.steps.forEach((item ) => (
                item.node = item.id < idx
                    ? ' done'
                    : (item.id === idx
                        ? ' active'
                        : '')
            ));
            this.isDisabled = this.backStatus();
            this.backStatus();
            this.buttonChange();
        },
        showDesc(idx) {
            this.title = steps[idx - 1].text;
        },
        setDefault() {
            this.activeIndex = 1;
            this.setActive(this.activeIndex);
            this.endButtonShow = false;
        },
        buttonChange(back = false) {
            if(this.activeIndex === this.steps.length) {
                this.backDisabled = 'disabled';
                this.endButtonShow = true;
            } else {
                this.backDisabled = '';
                this.endButtonShow = false;
            }
        },
        backStatus() {
            return this.activeIndex == 1;
        }
    },
    computed: {
        // тут стоит определить несколько свойств:
        // Q: реализовано через методы
        // 1. текущий выбранный шаг
        // 2. выключена ли кнопка назад
        // 3. находимся ли мы на последнем шаге
    },
    mounted() {
        this.setDefault();
    },
}

Vue.createApp(App).mount('#app')



